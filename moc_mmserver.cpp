/****************************************************************************
** Meta object code from reading C++ file 'mmserver.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../mastermind/source/mmserver.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mmserver.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MMServer_t {
    QByteArrayData data[5];
    char stringdata0[64];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MMServer_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MMServer_t qt_meta_stringdata_MMServer = {
    {
QT_MOC_LITERAL(0, 0, 8), // "MMServer"
QT_MOC_LITERAL(1, 9, 13), // "userConnected"
QT_MOC_LITERAL(2, 23, 0), // ""
QT_MOC_LITERAL(3, 24, 25), // "maximumConnectionsReached"
QT_MOC_LITERAL(4, 50, 13) // "newConnection"

    },
    "MMServer\0userConnected\0\0"
    "maximumConnectionsReached\0newConnection"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MMServer[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   29,    2, 0x06 /* Public */,
       3,    0,   30,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       4,    0,   31,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,

       0        // eod
};

void MMServer::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MMServer *_t = static_cast<MMServer *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->userConnected(); break;
        case 1: _t->maximumConnectionsReached(); break;
        case 2: _t->newConnection(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (MMServer::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MMServer::userConnected)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (MMServer::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MMServer::maximumConnectionsReached)) {
                *result = 1;
                return;
            }
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject MMServer::staticMetaObject = {
    { &MMConnection::staticMetaObject, qt_meta_stringdata_MMServer.data,
      qt_meta_data_MMServer,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *MMServer::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MMServer::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MMServer.stringdata0))
        return static_cast<void*>(const_cast< MMServer*>(this));
    return MMConnection::qt_metacast(_clname);
}

int MMServer::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = MMConnection::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
    return _id;
}

// SIGNAL 0
void MMServer::userConnected()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void MMServer::maximumConnectionsReached()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
