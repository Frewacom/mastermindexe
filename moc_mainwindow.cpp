/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../mastermind/source/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[23];
    char stringdata0[289];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 7), // "codeSet"
QT_MOC_LITERAL(2, 19, 0), // ""
QT_MOC_LITERAL(3, 20, 5), // "Code*"
QT_MOC_LITERAL(4, 26, 4), // "code"
QT_MOC_LITERAL(5, 31, 11), // "attemptMade"
QT_MOC_LITERAL(6, 43, 8), // "Response"
QT_MOC_LITERAL(7, 52, 4), // "resp"
QT_MOC_LITERAL(8, 57, 15), // "playerFoundCode"
QT_MOC_LITERAL(9, 73, 12), // "gameFinished"
QT_MOC_LITERAL(10, 86, 12), // "windowClosed"
QT_MOC_LITERAL(11, 99, 10), // "rightClick"
QT_MOC_LITERAL(12, 110, 6), // "sender"
QT_MOC_LITERAL(13, 117, 17), // "onColorBoxClicked"
QT_MOC_LITERAL(14, 135, 17), // "onFinishedClicked"
QT_MOC_LITERAL(15, 153, 19), // "onHighscoreAccepted"
QT_MOC_LITERAL(16, 173, 5), // "score"
QT_MOC_LITERAL(17, 179, 4), // "name"
QT_MOC_LITERAL(18, 184, 20), // "onHighscoreDismissed"
QT_MOC_LITERAL(19, 205, 13), // "onTimerUpdate"
QT_MOC_LITERAL(20, 219, 19), // "onGameOverDismissed"
QT_MOC_LITERAL(21, 239, 22), // "onColorBoxRightClicked"
QT_MOC_LITERAL(22, 262, 26) // "onCustomTextDialogFinished"

    },
    "MainWindow\0codeSet\0\0Code*\0code\0"
    "attemptMade\0Response\0resp\0playerFoundCode\0"
    "gameFinished\0windowClosed\0rightClick\0"
    "sender\0onColorBoxClicked\0onFinishedClicked\0"
    "onHighscoreAccepted\0score\0name\0"
    "onHighscoreDismissed\0onTimerUpdate\0"
    "onGameOverDismissed\0onColorBoxRightClicked\0"
    "onCustomTextDialogFinished"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   84,    2, 0x06 /* Public */,
       5,    2,   87,    2, 0x06 /* Public */,
       8,    0,   92,    2, 0x06 /* Public */,
       9,    0,   93,    2, 0x06 /* Public */,
      10,    0,   94,    2, 0x06 /* Public */,
      11,    1,   95,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      13,    0,   98,    2, 0x08 /* Private */,
      14,    0,   99,    2, 0x08 /* Private */,
      15,    2,  100,    2, 0x08 /* Private */,
      18,    0,  105,    2, 0x08 /* Private */,
      19,    0,  106,    2, 0x08 /* Private */,
      20,    0,  107,    2, 0x08 /* Private */,
      21,    1,  108,    2, 0x08 /* Private */,
      22,    0,  111,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 3, 0x80000000 | 6,    4,    7,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QObjectStar,   12,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::QString,   16,   17,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QObjectStar,   12,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->codeSet((*reinterpret_cast< Code*(*)>(_a[1]))); break;
        case 1: _t->attemptMade((*reinterpret_cast< Code*(*)>(_a[1])),(*reinterpret_cast< Response(*)>(_a[2]))); break;
        case 2: _t->playerFoundCode(); break;
        case 3: _t->gameFinished(); break;
        case 4: _t->windowClosed(); break;
        case 5: _t->rightClick((*reinterpret_cast< QObject*(*)>(_a[1]))); break;
        case 6: _t->onColorBoxClicked(); break;
        case 7: _t->onFinishedClicked(); break;
        case 8: _t->onHighscoreAccepted((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 9: _t->onHighscoreDismissed(); break;
        case 10: _t->onTimerUpdate(); break;
        case 11: _t->onGameOverDismissed(); break;
        case 12: _t->onColorBoxRightClicked((*reinterpret_cast< QObject*(*)>(_a[1]))); break;
        case 13: _t->onCustomTextDialogFinished(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (MainWindow::*_t)(Code * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::codeSet)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)(Code * , Response );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::attemptMade)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::playerFoundCode)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::gameFinished)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::windowClosed)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)(QObject * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::rightClick)) {
                *result = 5;
                return;
            }
        }
    }
}

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 14)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 14;
    }
    return _id;
}

// SIGNAL 0
void MainWindow::codeSet(Code * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void MainWindow::attemptMade(Code * _t1, Response _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void MainWindow::playerFoundCode()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void MainWindow::gameFinished()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void MainWindow::windowClosed()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void MainWindow::rightClick(QObject * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
