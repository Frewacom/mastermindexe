/****************************************************************************
** Meta object code from reading C++ file 'mmconnection.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../mastermind/source/mmconnection.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mmconnection.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MMConnection_t {
    QByteArrayData data[22];
    char stringdata0[306];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MMConnection_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MMConnection_t qt_meta_stringdata_MMConnection = {
    {
QT_MOC_LITERAL(0, 0, 12), // "MMConnection"
QT_MOC_LITERAL(1, 13, 21), // "connectionEstablished"
QT_MOC_LITERAL(2, 35, 0), // ""
QT_MOC_LITERAL(3, 36, 20), // "recievedPlayersReady"
QT_MOC_LITERAL(4, 57, 4), // "data"
QT_MOC_LITERAL(5, 62, 19), // "recievedChatMessage"
QT_MOC_LITERAL(6, 82, 3), // "msg"
QT_MOC_LITERAL(7, 86, 15), // "recievedAttempt"
QT_MOC_LITERAL(8, 102, 12), // "recievedCode"
QT_MOC_LITERAL(9, 115, 17), // "recievedPlayerWon"
QT_MOC_LITERAL(10, 133, 24), // "recievedConnectionDenied"
QT_MOC_LITERAL(11, 158, 10), // "selectCode"
QT_MOC_LITERAL(12, 169, 16), // "userDisconnected"
QT_MOC_LITERAL(13, 186, 15), // "youDisconnected"
QT_MOC_LITERAL(14, 202, 11), // "wroteToChat"
QT_MOC_LITERAL(15, 214, 11), // "errorHandle"
QT_MOC_LITERAL(16, 226, 5), // "error"
QT_MOC_LITERAL(17, 232, 9), // "connected"
QT_MOC_LITERAL(18, 242, 9), // "readyRead"
QT_MOC_LITERAL(19, 252, 12), // "disconnected"
QT_MOC_LITERAL(20, 265, 28), // "QAbstractSocket::SocketError"
QT_MOC_LITERAL(21, 294, 11) // "socketError"

    },
    "MMConnection\0connectionEstablished\0\0"
    "recievedPlayersReady\0data\0recievedChatMessage\0"
    "msg\0recievedAttempt\0recievedCode\0"
    "recievedPlayerWon\0recievedConnectionDenied\0"
    "selectCode\0userDisconnected\0youDisconnected\0"
    "wroteToChat\0errorHandle\0error\0connected\0"
    "readyRead\0disconnected\0"
    "QAbstractSocket::SocketError\0socketError"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MMConnection[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      16,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      12,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   94,    2, 0x06 /* Public */,
       3,    1,   95,    2, 0x06 /* Public */,
       5,    1,   98,    2, 0x06 /* Public */,
       7,    1,  101,    2, 0x06 /* Public */,
       8,    1,  104,    2, 0x06 /* Public */,
       9,    1,  107,    2, 0x06 /* Public */,
      10,    0,  110,    2, 0x06 /* Public */,
      11,    0,  111,    2, 0x06 /* Public */,
      12,    0,  112,    2, 0x06 /* Public */,
      13,    0,  113,    2, 0x06 /* Public */,
      14,    1,  114,    2, 0x06 /* Public */,
      15,    1,  117,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      17,    0,  120,    2, 0x0a /* Public */,
      18,    0,  121,    2, 0x0a /* Public */,
      19,    0,  122,    2, 0x0a /* Public */,
      16,    1,  123,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    4,
    QMetaType::Void, QMetaType::QString,    6,
    QMetaType::Void, QMetaType::QString,    4,
    QMetaType::Void, QMetaType::QString,    4,
    QMetaType::Void, QMetaType::QString,    4,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    6,
    QMetaType::Void, QMetaType::QString,   16,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 20,   21,

       0        // eod
};

void MMConnection::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MMConnection *_t = static_cast<MMConnection *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->connectionEstablished(); break;
        case 1: _t->recievedPlayersReady((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->recievedChatMessage((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: _t->recievedAttempt((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 4: _t->recievedCode((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 5: _t->recievedPlayerWon((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 6: _t->recievedConnectionDenied(); break;
        case 7: _t->selectCode(); break;
        case 8: _t->userDisconnected(); break;
        case 9: _t->youDisconnected(); break;
        case 10: _t->wroteToChat((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 11: _t->errorHandle((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 12: _t->connected(); break;
        case 13: _t->readyRead(); break;
        case 14: _t->disconnected(); break;
        case 15: _t->error((*reinterpret_cast< QAbstractSocket::SocketError(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 15:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAbstractSocket::SocketError >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (MMConnection::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MMConnection::connectionEstablished)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (MMConnection::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MMConnection::recievedPlayersReady)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (MMConnection::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MMConnection::recievedChatMessage)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (MMConnection::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MMConnection::recievedAttempt)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (MMConnection::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MMConnection::recievedCode)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (MMConnection::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MMConnection::recievedPlayerWon)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (MMConnection::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MMConnection::recievedConnectionDenied)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (MMConnection::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MMConnection::selectCode)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (MMConnection::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MMConnection::userDisconnected)) {
                *result = 8;
                return;
            }
        }
        {
            typedef void (MMConnection::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MMConnection::youDisconnected)) {
                *result = 9;
                return;
            }
        }
        {
            typedef void (MMConnection::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MMConnection::wroteToChat)) {
                *result = 10;
                return;
            }
        }
        {
            typedef void (MMConnection::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MMConnection::errorHandle)) {
                *result = 11;
                return;
            }
        }
    }
}

const QMetaObject MMConnection::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_MMConnection.data,
      qt_meta_data_MMConnection,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *MMConnection::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MMConnection::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MMConnection.stringdata0))
        return static_cast<void*>(const_cast< MMConnection*>(this));
    return QObject::qt_metacast(_clname);
}

int MMConnection::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    }
    return _id;
}

// SIGNAL 0
void MMConnection::connectionEstablished()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void MMConnection::recievedPlayersReady(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void MMConnection::recievedChatMessage(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void MMConnection::recievedAttempt(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void MMConnection::recievedCode(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void MMConnection::recievedPlayerWon(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void MMConnection::recievedConnectionDenied()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}

// SIGNAL 7
void MMConnection::selectCode()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}

// SIGNAL 8
void MMConnection::userDisconnected()
{
    QMetaObject::activate(this, &staticMetaObject, 8, nullptr);
}

// SIGNAL 9
void MMConnection::youDisconnected()
{
    QMetaObject::activate(this, &staticMetaObject, 9, nullptr);
}

// SIGNAL 10
void MMConnection::wroteToChat(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 10, _a);
}

// SIGNAL 11
void MMConnection::errorHandle(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 11, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
